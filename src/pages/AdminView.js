import { useEffect, useState, Fragment, useContext } from 'react'
import { Container, Table, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import UserContext from './../UserContext'

export default function AdminView() {

	const [allProducts, setAllProducts] = useState([])

	const { dispatch } = useContext(UserContext)

	const fetchData = () => {
		fetch(`http://localhost:3010/api/products`, {
			method: "GET",
			headers:{
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(response => {
			
			dispatch({type: "USER", payload: true})

			setAllProducts( response.map(product => {
				// console.log(course)

				return(
					<tr key={product._id}>
						<td>{product._id}</td>
						<td>{product.productName}</td>
						<td>{product.price}</td>
						<td>{product.isActive ? "Active" : "Inactive"}</td>
						<td>
							{
								product.isActive ?
									<Button 
										className="btn btn-danger mx-2"
										onClick={ () => handleArchive(product._id) }
									>
										Archive
									</Button>
								:
									<Fragment>
										<Button 
											className="btn btn-success mx-2"
											onClick={ () => handleUnarchive(product._id)}
										>
												Unarchive
										</Button>
										
									</Fragment>
							}
						</td>
					</tr>
				)
			}))
		})
	}

	useEffect(() => {
		fetchData()

	}, [])

	const handleArchive = (productId) =>{
		console.log(productId)
		fetch(`http://localhost:3010/api/products/${productId}/archive`, {
			method: "PATCH",
			headers:{
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(response => {
			console.log(response)

			if(response){
				fetchData()

				alert('Product successfully archived!')
			}
		})
	}

	const handleUnarchive = (productId) =>{
		console.log(productId)
		fetch(`http://localhost:3010/api/products/${productId}/unarchive`, {
			method: "PATCH",
			headers:{
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(response => {
			console.log(response)

			if(response){
				fetchData()
				
				alert('Product successfully Unarchived!')
			}
		})
	}

	

	return(
		<Container className="container">
			<h1 className="my-5 p-3 text-center">Product Dashboard</h1>
			<div className="text-right border">
				<Link className="btn btn-dark m-2" to={`/addCourse`}>Add Product</Link>
				<Link className="btn btn-dark m-2" to={`/access`}>User Dashboard</Link>
			</div>
			<Table className= "border">
				<thead>
					<tr>
						<th>ID</th>
						<th>Product Name</th>
						<th>Price</th>
						<th>Status</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{ allProducts }
				</tbody>
			</Table>
		</Container>
	)
}