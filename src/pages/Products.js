import {Fragment, useState, useContext, useEffect} from  'react'
import UserContext from './../UserContext'
import AdminView from './AdminView'
import ProductCard from './../components/ProductCard'
const admin = localStorage.getItem('admin')
const token = localStorage.getItem('token')


export default function Products () {

	const {state, dispatch} = useContext (UserContext)

	const [products, setProducts] = useState([])

	


	useEffect ( () => {
		if(admin === "false"){
			fetch ('http://localhost:3010/api/products/isActive', {
				method: "GET",
				headers:{
					"Authorization": `Bearer ${token}`
				}

			})
			.then(response => response.json())
			.then(response => {
				if (token !== null) {
					dispatch({type:"USER", payload: true})
				}

				setProducts (
					response.map(product => {
						return <ProductCard key = {
							product._id} productProp= {product}/>
						
					})
				)
			})

		}

	}, [])

	return  (

		<Fragment>
			{
				admin === "false" ?
					<Fragment>
						{products}

					</Fragment>
				:
					<Fragment>
						<AdminView/>
					</Fragment>
			}

		</Fragment>
	)
}