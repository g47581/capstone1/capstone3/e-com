import { useReducer, useEffect  } from 'react';
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";

import { Container } from 'react-bootstrap'

import { UserProvider } from './UserContext'
import { initialState, reducer } from './reducer/UserReducer'

import AppNavbar from './components/AppNavbar'
import Footer from './components/Footer'

import ErrorPage from './pages/ErrorPage'
import AdminView from './pages/AdminView'

import Register from './pages/Register'
import Home from './pages/Home'
import Products from './pages/Products'
import Login from './pages/Login'
import Logout from './pages/Logout'



function App() {
  const [state , dispatch] = useReducer(reducer, initialState)
  return (
    

    <UserProvider value = {{state, dispatch}}>
      <BrowserRouter >
        <AppNavbar/>
        <div className="m-5"></div>
        <Routes >
          <Route path = "/" element={ <Home/> } />
          <Route path = "/products" element={ <Products/> } />}
          <Route path = "/register" element={ <Register/> } />
          <Route path="*" element={ <ErrorPage/> } />

          <Route path="/login" element={ <Login/> } />
          <Route path = "/logout" element ={<Logout/> } />

          
        </Routes>
        <Footer/>
      </BrowserRouter>
    </UserProvider>

   )
}

export default App;
